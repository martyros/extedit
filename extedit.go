// Extedit allows command-line utilities to spin up an external editor.
//
// By default, the EDITOR environment variable is used.  The caller can specify an editor
// using the EditCmd variable.  This type satisfies both the flag.Value and pflag.Value interfaces,
// so can be passed into those to allow the user to opitonally set the editor.
//
// For example:
//   flag.Var(extedit.EditCmd, "editor", "Editor command")
package extedit

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"gopkg.in/yaml.v3"
)

type StringValue struct {
	value string
}

func (sv *StringValue) Set(value string) error {
	sv.value = value
	return nil
}

func (sv StringValue) String() string {
	return sv.value
}

func (sv StringValue) Get() interface{} {
	return sv.value
}

func (sv StringValue) Type() string {
	return "string"
}

// Call EditCmd.Set("cmd") to set the edit command.  This implements
// both the flag.Value and pflag.Value interfaces, so can be used both
// with the native golang flag package, and github.com/spf13/pflag.
var EditCmd StringValue

// EditFile will open up a file named 'filename' in an external
// editor.
func EditFile(filename string) error {
	if EditCmd.value == "" {
		var prs bool
		EditCmd.value, prs = os.LookupEnv("EDITOR")
		if !prs {
			return fmt.Errorf("Please set EDITOR environment variable")
		}
	}

	cmd := exec.Command(EditCmd.value, filename)

	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		err = fmt.Errorf("Running editor (%s): %v", EditCmd.value, err)
		return err
	}

	return nil
}

// EditBytes will write bytes out to a temporary file, call EditFile
// on that file, and afterwards read that file back in and return the
// result.
func EditBytes(out []byte) ([]byte, error) {
	// Open temporary file
	tmpfile, err := ioutil.TempFile("", "edit*.txt")
	if err != nil {
		return nil, fmt.Errorf("Creating temp file for segmentation: %v", err)
	}

	// Write the output to the file
	_, err = tmpfile.Write(out)
	if err != nil {
		return nil, fmt.Errorf("Writing text to file: %v", err)
	}

	// Close the file before editing...
	tmpfileName := tmpfile.Name()
	tmpfile.Close()

	// Run the external editor.  This will do its own errors if there's a failure.
	err = EditFile(tmpfileName)
	if err != nil {
		return nil, err
	}

	// Read the file back in
	in, err := ioutil.ReadFile(tmpfileName)
	if err != nil {
		return nil, fmt.Errorf("Reading back tempfile after edit: %v", err)
	}

	return in, nil
}

type yamlOpts struct {
	message string
}

type YamlOpt func(*yamlOpts) error

// The Message option will cause EditYaml to insert the message as a
// YAML comment at the top of the file to be edited.  Line breaks are
// preserved.
//
// For example:
//  obj := struct { Name string, Count int } {}
//  extedit.EditYaml(obj, extedit.Message("This is\na test\n")
//
// Will open the following with $EDITOR:
//  # This is
//  # a test
//  name: ""
//  count: 0
func Message(message string) YamlOpt {
	return func(yopt *yamlOpts) error {
		yopt.message = message
		return nil
	}
}

// EditYaml allows you to call an exeternal editor to edit a data
// structure with the external editor.  It will marshal the data
// structure into yaml, write the output to a temporary file, call the
// external editor; then it will read the file back, and unmarshal the
// data back into the same structure.
//
// Obviously the structure needs to be of a reference type, or the
// unmarshal will fail.
//
// This package uses https://gopkg.in/yaml.v3 to do YAML marshalling
// and unmarshalling.
func EditYaml(out interface{}, opt ...YamlOpt) error {
	var opts yamlOpts

	for _, f := range opt {
		if err := f(&opts); err != nil {
			return fmt.Errorf("Processing options: %v", err)
		}
	}

	b, err := yaml.Marshal(out)
	if err != nil {
		return err
	}

	if opts.message != "" {
		// Comment the message
		c := "# " + opts.message
		// If it ended with a newline, remove it, so we don't comment the
		// next line
		c = strings.TrimSuffix(c, "\n")

		c = strings.ReplaceAll(c, "\n", "\n# ")

		// Then add a newline back
		c = c + "\n"
		// Add to b
		b = append([]byte(c), b...)
	}

	b, err = EditBytes(b)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(b, out)
	return err
}
